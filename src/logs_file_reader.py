import os
import logging
import pandas as pd
import sqlite3


LINES_NUMBER_TO_READ = 100
LOGS_FILE_PATH = "/var/log/audit/audit.log"
POSITION_FILE_PATH = "position.txt"
DB_NAME = "audit.db"

connection = sqlite3.connect(DB_NAME)

def read_logs():
    position = get_position()
    with open(LOGS_FILE_PATH, "rb") as logs:
        logs.seek(position)
        content = logs.readline()
        new_position = logs.tell()
        update_position(new_position)
        return content
    

def get_position():
    try:
        if not os.path.exists(POSITION_FILE_PATH):
            return 0
        with open(POSITION_FILE_PATH, "r") as position_file:
            position = int(position_file.read())
            return position
    except:
        logging.exception("Failed to get position")
        return 0
    

def update_position(position):
    with open(POSITION_FILE_PATH, "w") as position_file:
        position_file.write(str(position))


def parse_log(log):
    try:
        data = log.replace('\x1d'.encode(), ' '.encode())
        data_string = data.decode().replace('\n','').split(" ")
        parsed_log = {}
        for key_value in data_string:
            try:
                [key, value] = key_value.split("=")
                parsed_log[key.lower()] = value
            except:
                logging.exception(f"Failed to parse key value pair: {key_value}")
        return parsed_log
    except:
        logging.exception("Failed to parse log")
        return {}

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    results = []
    for i in range(LINES_NUMBER_TO_READ):
        content = read_logs()
        logging.info(content)
        parsed_log = parse_log(content)
        logging.info(parsed_log)
        results.append(parsed_log)
    result_df = pd.DataFrame(results)
    logging.info(result_df)
    result_df.to_sql(name='raw_messages', con=connection, if_exists="append")
    logging.info("DONE!!!")
