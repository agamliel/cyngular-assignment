-- The number of different "auditd" rules/commands in the database.
select count(distinct key) from raw_messages;
-- The rule that appears most frequently.
select key, count(*) as count from raw_messages group by key order by count desc limit 1;
-- The command that appears most frequently.
select comm, count(*) as count from raw_messages group by comm order by count desc limit 1;
-- The user who executes the most commands.
select auid, count(*) as count from raw_messages group by auid order by count desc limit 1;
-- The least common command.
select comm, count(*) as count from raw_messages group by comm order by count asc limit 1;
-- The folder path that has the most activity.
select cwd, count(*) as count from raw_messages group by cwd order by count desc limit 1;